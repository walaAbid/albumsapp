import { createStore, applyMiddleware, combineReducers, compose } from "redux";
import thunk from "redux-thunk";
import reducerAlbums from "./reducersCatalog/reducerAlbums";
import reducerPhotos from "./reducersCatalog/reducerPhotos";
import reducerWishlist from "./reducersCatalog/reducerwishlist"
import reducerDétails from "./reducersCatalog/reducerDetails";

const middleware = [thunk];
const store = createStore(
  combineReducers({reducerAlbums, reducerPhotos,reducerWishlist,reducerDétails}),
  compose(
    applyMiddleware(...middleware),
    window.__REDUX_DEVTOOLS_EXTENSION__()
  )
);
export default store;
