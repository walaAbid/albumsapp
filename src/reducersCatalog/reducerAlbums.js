import {GET_ALBUM } from "../actions/actionType";
const initialeState = [];

const reducerAlbums = (state = initialeState, action) => {
  switch (action.type) {
    case GET_ALBUM:
      return action.payload;
    default:
      return state;
  }
};
export default reducerAlbums;
