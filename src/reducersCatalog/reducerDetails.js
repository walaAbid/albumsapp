import { DETAILS_PHOTO } from "../actions/actionType";
const initialeState = [];

const reducerDétails = (state = initialeState, action) => {
  switch (action.type) {
    case DETAILS_PHOTO:
      return action.payload;

    default:
      return state;
  }
};
export default reducerDétails;
