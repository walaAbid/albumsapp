import { GET_PHOTO, DELETE_PHOTO, DETAILS_PHOTO } from "../actions/actionType";
const initialeState = [];

const reducerPhotos = (state = initialeState, action) => {
  switch (action.type) {
    case GET_PHOTO:
      return action.payload;
    case DELETE_PHOTO:
      return state.filter(el => el.id !== action.id);

    case DETAILS_PHOTO:
      return state;

    default:
      return state;
  }
};
export default reducerPhotos;
