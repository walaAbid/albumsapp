import {ADD_wishlist,GET_WICHLIST,DELETE_FAVORIT} from "../actions/actionType";
const initialeState = [ ];

const reducerWishlist = (state = initialeState, action) => {
  switch (action.type) {
    case ADD_wishlist:
      return state.concat(action.payload)
    case GET_WICHLIST:
      return state
      case DELETE_FAVORIT:
        return state.filter(t => t !== action.payload)
    default:
      return state;
  }
};
export default reducerWishlist;
