import React from "react";
import { connect } from "react-redux";
import { Route } from "react-router-dom";
import { getAlbums } from "./actions/action";

import "./App.css";
import PhotoDétails from "./component/PhotoDétails" 
import Acceuil from "./component/Aceuil";
import PhotoListpage from "./component/PhotoListpage";
import wishlist from './component/WishList'

class App extends React.Component {
  componentDidMount = () => {
    this.props.getAlbums();
  };
  render() {
    return (
      <div className="App">
       
          <Route exact path="/" render={() => <Acceuil />} />
          {/* id={props.match.params.id} */}
          <Route path="/photoalbum/:id" component={PhotoListpage} />
          <Route path="/photoalbum/:id" component={PhotoListpage} />
          <Route path="/photoalbum" component={PhotoListpage} />
          <Route path="/wishlist" component={wishlist} />
          <Route path="/photodetails" component={PhotoDétails} />


        
      </div>
    );
  }
}
const mapDispatchToProps = dispatch => {
  return {
    getAlbums: () => dispatch(getAlbums()),
  };
};
export default connect(
  null,
  mapDispatchToProps
)(App);
