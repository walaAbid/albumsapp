import React from "react";
import { Theme, createStyles, makeStyles } from "@material-ui/core/styles";
import GridList from "@material-ui/core/GridList";
import GridListTile from "@material-ui/core/GridListTile";
import Typography from "@material-ui/core/Typography";
import AlbumCard from "./albumCard";

import { connect } from "react-redux";
import Spinner from "./Spinner";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: "flex",
      flexWrap: "wrap",
      justifyContent: "center",
      overflow: "hidden",
      backgroundColor: "white",
      width: "80%",
      margin: "10px auto"
    },
    album: {
      color: "#787878",
      margin: 20,
      padding: 10,
      borderBottom: "solid"
    },
    bar: {
      borderRadius: 6,
      background:
        "linear-gradient(to bottom, rgba(0,0,0,0.7) 0%, " +
        "rgba(0,0,0,0.3) 70%, rgba(0,0,0,0) 100%)"
    },

    gridList: {
      height: "auto",
      justifyContent: "center"
    },
    grid: {
      borderRadius: 40
    },
    icon: {
      color: "rgba(255, 255, 255, 0.54)"
    }
  })
);

function Aceuil(props) {
  const classes = useStyles();

  return !props.albums ? (
    <Spinner />
  ) : (
    <div className={classes.root}>
      <GridList cellHeight={180} className={classes.gridList}>
        <GridListTile key="Subheader" cols={2} style={{ height: "auto" }}>
          <Typography
            className={classes.album}
            component="h1"
            variant="h2"
            align="center"
            color="textPrimary"
            gutterBottom
          >
            Albums
          </Typography>
        </GridListTile>

        {props.albums.map(tile => (
          <AlbumCard className={classes.grid} tile={tile} />
        ))}
      </GridList>
    </div>
  );
}

const mapStateToProps = state => {
  return { albums: state.reducerAlbums };
};

export default connect(mapStateToProps)(Aceuil);
