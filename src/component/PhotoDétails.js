import React from "react";
import { makeStyles, Theme, createStyles } from "@material-ui/core/styles";

import Card from "@material-ui/core/Card";
import CardHeader from "@material-ui/core/CardHeader";
import CardMedia from "@material-ui/core/CardMedia";
import CardContent from "@material-ui/core/CardContent";
import CardActions from "@material-ui/core/CardActions";
import Typography from "@material-ui/core/Typography";
import GridListTile from "@material-ui/core/GridListTile";
import BottomNavigation from "@material-ui/core/BottomNavigation";
import BottomNavigationAction from "@material-ui/core/BottomNavigationAction";
import RestoreIcon from "@material-ui/icons/Restore";

import { Link } from "react-router-dom";
import { connect } from "react-redux";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    card: {
      width: "25%",
      height: "10%",
      margin: "2%",
      position: "relative",
      left: "10%",
      minWidth: 200,
      display: "flex",
      flexDirection: "column",
      justifyContent: "center"
    },
    media: {
      height: 0,
      paddingTop: "56.25%" // 16:9
    },
    expand: {
      transform: "rotate(0deg)",
      marginLeft: "auto",
      transition: theme.transitions.create("transform", {
        duration: theme.transitions.duration.shortest
      })
    },
    détails: {
      color: "#787878",
      margin: 20,
      padding: 10,
      borderBottom: "solid"
    },
    bar: {
      borderRadius: 15,
      background:
        "linear-gradient(to bottom, rgba(0,0,0,0.7) 0%, " +
        "rgba(0,0,0,0.3) 70%, rgba(0,0,0,0) 5%)"
    },
    expandOpen: {
      transform: "rotate(180deg)"
    },

    cardActions: {
      display: "flex",
      padding: "8px",
      alignItems: "center",
      justifyContent: "space-between"
    }
  })
);

function RecipeReviewCard(props) {
  const classes = useStyles();
  const [value, setValue] = React.useState(0);
  const { photoDétails } = props;

  return (
    <div
      style={{
        display: "flex",
        justifyContent: "center",
        flexDirection: "column"
      }}
    >
      <GridListTile key="Subheader" cols={2} style={{ height: "auto" }}>
        <Typography
          className={classes.détails}
          component="h1"
          variant="h2"
          align="center"
          color="textPrimary"
          gutterBottom
        >
          Détails
        </Typography>
      </GridListTile>

      <Card className={classes.card}>
        <Link to="/photoalbum">
          <BottomNavigation
            value={value}
            onChange={(event, newValue) => {
              setValue(newValue);
            }}
            showLabels
            className={classes.root}
          >
            <BottomNavigationAction label="Recents" icon={<RestoreIcon />} />
          </BottomNavigation>
        </Link>
        <CardHeader title={photoDétails.title} />
        <CardMedia className={classes.media} image={photoDétails.url} />
        <CardContent />
        <CardActions style={{ display: "flex", justifyContent: "center" }} />
      </Card>
    </div>
  );
}
const mapStateToProps = state => {
  return { photoDétails: state.reducerDétails };
};

export default connect(mapStateToProps)(RecipeReviewCard);
