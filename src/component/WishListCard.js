import React from "react";
import { connect } from "react-redux";

import { makeStyles, Theme, createStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardHeader from "@material-ui/core/CardHeader";
import CardMedia from "@material-ui/core/CardMedia";
import CardContent from "@material-ui/core/CardContent";
import CardActions from "@material-ui/core/CardActions";
import IconButton from "@material-ui/core/IconButton";
import DeleteIcon from "@material-ui/icons/Delete";

import { deleteFavorit } from "../actions/action";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    card: {
      width: "25%",
      height: "10%",
      margin: "2%",
      position: "relative",
      left: "10%",
      minWidth: 200,
      display: "flex",
      flexDirection: "column",
      justifyContent: "center"
    },
    media: {
      height: 0,
      paddingTop: "56.25%" // 16:9
    },
    expand: {
      transform: "rotate(0deg)",
      marginLeft: "auto",
      transition: theme.transitions.create("transform", {
        duration: theme.transitions.duration.shortest
      })
    },
    expandOpen: {
      transform: "rotate(180deg)"
    },
    cardActions: {
      display: "flex",
      padding: "8px",
      alignItems: "center",
      justifyContent: "space-between"
    }
  })
);

function RecipeReviewCard(props) {
  const classes = useStyles();
  const { url, title } = props.tile;

 
  return (
    <Card className={classes.card}>
      <CardActions className={classes.cardActions}>
        <IconButton
          aria-label="delete"
          className={classes.margin}
          onClick={() => {
            props.deleteFavorit(props.tile);
          }}
        >
          <DeleteIcon fontSize="small" />
        </IconButton>
      </CardActions>
      <CardHeader title={title} />
      <CardMedia className={classes.media} image={url} />
      <CardContent />
    </Card>
  );
}
const mapDispatchToProps = dispatch => {
  return { deleteFavorit: payload => dispatch(deleteFavorit(payload)) };
};
export default connect(
  null,
  mapDispatchToProps
)(RecipeReviewCard);
