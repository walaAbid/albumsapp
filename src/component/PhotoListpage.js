import React, { useEffect } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";

import { Theme, createStyles, makeStyles } from "@material-ui/core/styles";
import GridList from "@material-ui/core/GridList";
import GridListTile from "@material-ui/core/GridListTile";
import Fab from "@material-ui/core/Fab";
import NavigationIcon from "@material-ui/icons/Navigation";
import Typography from "@material-ui/core/Typography";
import Photocards from "./photoscards";

import Spinner from "./Spinner";
import { getPhotos, getWishlist } from "../actions/action";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: "flex",
      flexFlow: "row wrap",
      justifyContent: "center",
      overflow: "hidden",
      backgroundColor: "white",
      width: "80%",
      margin: "20px auto"
    },
    photo: {
      color: "#787878",
      margin: 20,
      padding: 10,
      borderBottom: "solid"
    },
    bar: {
      borderRadius: 6,
      background:
        "linear-gradient(to bottom, rgba(0,0,0,0.7) 0%, " +
        "rgba(0,0,0,0.3) 70%, rgba(0,0,0,0) 100%)"
    },

    icon: {
      color: "rgba(255, 255, 255, 0.54)"
    }
  })
);

function PhotoListpage(props) {
  useEffect(() => {
    props.getPhotos(props.match.params.id);
  }, []);

  return !props.photos ? (
    <Spinner />
  ) : (
    <div className={useStyles.root}>
      <GridList cellHeight={180} className={useStyles.gridList}>
        <GridListTile key="Subheader" cols={2} style={{ height: "auto" }}>
          <Typography
            className={useStyles.photo}
            component="h1"
            variant="h2"
            align="center"
            color="textPrimary"
            gutterBottom
          >
            Photo
          </Typography>
        </GridListTile>

        {props.photos.map(tile => (
          <Photocards className={useStyles.grid} tile={tile} />
        ))}
      </GridList>
      <Link
        to="/wishlist"
        onClick={() => {
          props.getWishlist();
        }}
      >
        <Fab
          variant="extended"
          size="small"
          color="primary"
          aria-label="add"
          className={useStyles.margin}
        >
          <NavigationIcon className={useStyles.extendedIcon} />
          Add To Wishlist
        </Fab>
      </Link>
    </div>
  );
}

const mapStateToProps = state => {
  return { photos: state.reducerPhotos };
};

const mapDispatchToProps = dispatch => {
  return {
    getPhotos: id => dispatch(getPhotos(id)),
    getWishlist: payload => dispatch(getWishlist(payload))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PhotoListpage);
