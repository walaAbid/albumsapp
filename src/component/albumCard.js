import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Button from "@material-ui/core/Button";
import { Link } from "react-router-dom";

import Typography from "@material-ui/core/Typography";

const useStyles = makeStyles({
  card: {
    width: "30%",
    margin: "1%",
    minWidth: 300,
    display: "flex",
    flexDirection: "column",
    justifyContent: "space-between"
  },
  media: {
    height: -7
  }
});

function MediaCard(props) {
  const classes = useStyles();
  const { img, title, id } = props.tile;
  return (
    <Card className={classes.card}>
      <CardActionArea>
        <CardMedia className={classes.media} image={img} title={title} />
        <CardContent>
          <Typography gutterBottom variant="h5" component="h2">
            {title}
          </Typography>
        </CardContent>
      </CardActionArea>
      <CardActions style={{ display: "flex", justifyContent: "center" }}>
        <Link to={`/photoalbum/${id}`}>
          <Button size="small" color="primary">
            View Gallery
          </Button>
        </Link>
      </CardActions>
    </Card>
  );
}

export default MediaCard;
