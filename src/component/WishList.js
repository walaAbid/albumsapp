import React, { useEffect } from "react";
import { connect } from "react-redux";

import { Theme, createStyles, makeStyles } from "@material-ui/core/styles";
import GridList from "@material-ui/core/GridList";
import GridListTile from "@material-ui/core/GridListTile";
import GridListTileBar from "@material-ui/core/GridListTileBar";
import ListSubheader from "@material-ui/core/ListSubheader";
import IconButton from "@material-ui/core/IconButton";
import InfoIcon from "@material-ui/icons/Info";
import Typography from "@material-ui/core/Typography";
import Wichlistcards from "./WishListCard";
import Spinner from "./Spinner";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: "flex",
      flexFlow: "row wrap",
      justifyContent: "center",
      overflow: "hidden",
      backgroundColor: "white",
      width: "80%",
      margin: "20px auto"
    },
    album: {
      color: "#787878",
      margin: 20,
      padding: 10,
      borderBottom: "solid"
    },
    bar: {
      borderRadius: 6,
      background:
        "linear-gradient(to bottom, rgba(0,0,0,0.7) 0%, " +
        "rgba(0,0,0,0.3) 70%, rgba(0,0,0,0) 100%)"
    },

    icon: {
      color: "rgba(255, 255, 255, 0.54)"
    }
  })
);

function wishlist(props) {
  //   useEffect(() => {
  //     props.getPhotos(props.match.params.id);
  //   }, []);
  const { wishlist } = props;

  return !props.wishlist ? (
    <Spinner />
  ) : (
    <div className={useStyles.root}>
      <GridList cellHeight={180} className={useStyles.gridList}>
        <GridListTile
          key="Subheader"
          cols={2}
          style={{ height: "auto" }}
          className={useStyles.gridListtile}
        >
          <Typography
            className={useStyles.album}
            component="h1"
            variant="h2"
            align="center"
            color="textPrimary"
            gutterBottom
          >
            wishlist
          </Typography>
        </GridListTile>

        {wishlist.map(tile => (
          <Wichlistcards className={useStyles.grid} tile={tile} />
        ))}
      </GridList>
    </div>
  );
}

const mapStateToProps = state => {
  return { wishlist: state.reducerWishlist };
};

export default connect(
  mapStateToProps
)(wishlist);
