import React from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";

import { makeStyles, Theme, createStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardHeader from "@material-ui/core/CardHeader";
import CardMedia from "@material-ui/core/CardMedia";
import CardContent from "@material-ui/core/CardContent";
import CardActions from "@material-ui/core/CardActions";
import IconButton from "@material-ui/core/IconButton";
import FavoriteIcon from "@material-ui/icons/Favorite";
import DeleteIcon from "@material-ui/icons/Delete";
import Button from "@material-ui/core/Button";

import { addWishlist, deletePhotos, détailsPhoto } from "../actions/action";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    card: {
      width: "25%",
      height: "10%",
      margin: "2%",
      position: "relative",
      left: "10%",
      minWidth: 200,
      display: "flex",
      flexDirection: "column",
      justifyContent: "center"
    },
    media: {
      height: 0,
      paddingTop: "56.25%" // 16:9
    },
    expand: {
      transform: "rotate(0deg)",
      marginLeft: "auto",
      transition: theme.transitions.create("transform", {
        duration: theme.transitions.duration.shortest
      })
    },
    expandOpen: {
      transform: "rotate(180deg)"
    },
    cardActions: {
      display: "flex",
      padding: "8px",
      alignItems: "center",
      justifyContent: "space-between"
    }
  })
);

function RecipeReviewCard(props) {
  const classes = useStyles();
  const { url, title, id } = props.tile;

  return (
    <Card className={classes.card}>
      <CardActions className={classes.cardActions}>
        <IconButton
          aria-label="add to favorites"
          onClick={() => {
            props.addWishlist({ url, title});
          }}
        >
          <FavoriteIcon />
        </IconButton>
        <IconButton
          aria-label="delete"
          className={classes.margin}
          onClick={() => {
            props.deletePhotos(id);
          }}
        >
          <DeleteIcon fontSize="small" />
        </IconButton>
      </CardActions>
      <CardHeader title={title} />
      <CardMedia className={classes.media} image={url} />
      <CardContent />
      <CardActions style={{ display: "flex", justifyContent: "center" }}>
        <Link
          to="/photodetails"
          onClick={() => {
            props.détailsPhoto(props.tile);
          }}
        >
          <Button size="small" color="primary">
            View Détails
          </Button>
        </Link>
      </CardActions>
    </Card>
  );
}
const mapDispatchToProps = dispatch => {
  return {
    addWishlist: payload => dispatch(addWishlist(payload)),
    deletePhotos: payload => dispatch(deletePhotos(payload)),
    détailsPhoto: (url, title) => dispatch(détailsPhoto(url, title))
  };
};

export default connect(
  null,
  mapDispatchToProps
)(RecipeReviewCard);
