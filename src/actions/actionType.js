// getting albums from json file

export const GET_ALBUM = "GET_ALBUM";

// getting photos from json file
export const GET_PHOTO = "GET_PHOTO";
export const DELETE_PHOTO = "DELETE_PHOTO";

// wishlist
export const ADD_wishlist = "ADD_wishlist";
export const GET_WICHLIST = "GET_WICHLIST";
export const DELETE_FAVORIT = "DELETE_FAVORIT";
export const DETAILS_PHOTO = "DETAILS_PHOTO";
