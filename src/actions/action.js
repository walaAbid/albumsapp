import {
  GET_ALBUM,
  GET_PHOTO,
  ADD_wishlist,
  GET_WICHLIST,
  DELETE_FAVORIT,
  DELETE_PHOTO,
  DETAILS_PHOTO
} from "./actionType";

export const getAlbums = () => dispatch => {
  fetch("https://jsonplaceholder.typicode.com/albums")
    .then(response => response.json())
    .then(json =>
      dispatch({
        type: GET_ALBUM,
        payload: json
      })
    )
    .catch(err => console.log(err));
};

export const getPhotos = id => dispatch => {
  fetch(`https://jsonplaceholder.typicode.com/photos/?albumId=${id}`)
    .then(response => response.json())
    .then(json =>
      dispatch({
        type: GET_PHOTO,
        payload: json
      })
    )
    .catch(err => console.log(err));
};
export const deletePhotos = id => {
  return {
    type: DELETE_PHOTO,
    id
  };
};
export const addWishlist = payload => {
  return {
    type: ADD_wishlist,
    payload
  };
};
export const getWishlist = payload => {
  return {
    type: GET_WICHLIST,
    payload
  };
};
export const deleteFavorit = payload => {
  return {
    type: DELETE_FAVORIT,
    payload
  };
};
export const détailsPhoto = payload => {
  return {
    type: DETAILS_PHOTO,
    payload
  };
};
